import Vue from "vue";
import Vuex from "vuex";
import firebase from "firebase";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    user: false,
    username: ""
  },
  mutations: {
    setUsername: (state, payload) => {
      state.username = payload;
    },
    setUser: (state, payload) => {
      state.user = payload;
    }
  },
  actions: {
    setUser({ commit }) {
      const user = firebase.auth().currentUser;
      if (user) {
        commit("setUsername", user.displayName);
        commit("setUser", true);
      }
    },
    resetUser({ commit }) {
      commit("setUsername", "");
      commit("setUser", false);
    }
  }
});
