import Vue from "vue";
import "./plugins/vuetify";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import firebase from "firebase";
import "./registerServiceWorker";

Vue.config.productionTip = false;

const config = {
  apiKey: "AIzaSyBnEnPxI4MI_NALeQFUWOQJ5ALVGLpC6iI",
  authDomain: "timesheetx-1428c.firebaseapp.com",
  databaseURL: "https://timesheetx-1428c.firebaseio.com/",
  projectId: "timesheetx-1428c"
};

firebase.initializeApp(config);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
