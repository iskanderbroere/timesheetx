import Vue from "vue";
import Router from "vue-router";
import Login from "./views/Login.vue";
import TimeSheet from "./views/TimeSheet";
import firebase from "firebase";

Vue.use(Router);

const router = new Router({
  mode: "history",
  routes: [
    // {
    //   path: "*",
    //   name: "login",
    //   component: Login
    // },
    {
      path: "/",
      name: "login",
      component: Login
    },
    {
      path: "/timesheet",
      name: "timesheet",
      component: TimeSheet,
      meta: {
        requiresAuth: true
      }
    }
  ]
});

router.beforeEach((to, from, next) => {
  firebase.auth().onAuthStateChanged(user => {
    let currentUser = false;
    if (user) {
      currentUser = true;
    }
    const requiresAuth = to.matched.some(record => record.meta.requiresAuth);

    if (requiresAuth && !currentUser) next("/");
    if (!requiresAuth && currentUser) next("timesheet");
    next();
  });
});
export default router;
